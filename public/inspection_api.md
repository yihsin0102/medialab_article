# 你的網站需要全身健檢！初探Google URL Inspection API

**文：中央社媒體實驗室**

2022年1月31日，Google搜尋中心發表新的API，叫[URL Inspection API](https://developers.google.com/search/blog/2022/01/url-inspection-api?hl=zh_tw)，讓開發人員可以批次健檢自家網站的網址，及時確認索引狀態，並進行偵錯與處理。

## 什麼樣的網站適合？

- 每天都會生成千百個網址
- 想針對大量網址做健檢
- 相當重視即時搜尋成效
- 想優化網站SEO

## URL檢查工具是什麼？

當你發布一個網址，想確認Google大神有沒有注意到你的內容時，可以把這個網址丟進去查看，如果顯示勾勾，就代表祂已讀了，有人搜尋相關關鍵字時，你的網頁就會有效出現在搜尋結果中。

## 這支API還可以查出什麼資訊？

- Google爬到的內容有哪些？
- 檢索時間、檢索狀態、網頁擷取狀態
- Google選到的標準網址是什麼？
- 網頁是否適合用行動裝置瀏覽？
- AMP網頁是否有效？
- Google抓到的結構化資料長怎樣？
- robots.txt 是否封鎖該頁面？

![Google search console中的網址檢查工具](img\01_GSC.jpg "Google search console中的網址檢查工具")

但網址一個一個輸入太麻煩，加上我們時刻都會發布新報導，需要立即被檢索，此時這隻API就大量、即時地呈現Google的檢索狀態。


## 串接步驟

我們這次是使用Python來串的！

1. 權杖設定

 前往[Google Cloud Platform](https://console.cloud.google.com/home)建立專案，點選「啟用API和服務」並導入[Google Search Console API](https://console.cloud.google.com/apis/library/searchconsole.googleapis.com)。關於管理API金鑰，詳細中文資源可參考：[JumpingCode 資料科學手記](https://jumping-code.com/2021/05/27/google-search-console-api-%E6%95%99%E5%AD%B8/)

 這塊最麻煩的地方是要申請服務帳戶，以及OAth 2.0用戶端ID（Client ID），把服務帳戶的權限改為「完整」或「擁有者」，並新增金鑰，你會得到一個json檔，包含以下資料：（注意不可以外流）
 
 ![權杖設定](img\02_python.jpg "權杖設定")

2. 安裝Python套件

 **串接API必要套件**

 - ```pip3 install --upgrade google-auth```

 -  ```pip3 install google-api-python-client==1.12.10```

 - ```pip3 install requests```

 完成後就可以開始寫code串接哩！


3. 在資料夾中放入上述json檔，並新增一個py檔

[filename](inspection_api_code\python_code.py ':include :type=code :fragment=demo')


4. 設定權杖

[filename](inspection_api_code\python_code.py ':include :type=code :fragment=abc')

5. 告訴API你要哪些資料，他就會回傳相對應的INSPECTION資料

[filename](inspection_api_code\python_code.py ':include :type=code :fragment=demo2')

測試沒問題後，就可以寫迴圈，抓你需要的資料嚕！


## 串接注意事項

- 配額限制
 每天 2,000 個查詢
 每分鐘 600 個查詢

- HTTP Error 500
 批量呼叫API時，爬蟲不定期出現這則訊息並終止運作：
 
 ```googleapiclient.errors.HttpError: <HttpError 500 when requesting https://searchconsole.googleapis.com/v1/urlInspection/index:inspect?alt=json returned "Internal error encountered.". Details: "Internal error encountered.">```

 意思是因為Google內部錯誤，它無法吐回我們請求的資料。我們就此問題到Search Console Community尋求協助，專家的回覆是：

 > 我們難以避免這個問題，API閘道系統背後有約1000台伺服器在處理各種請求，若有其中1台出現問題，便會回傳Error 500，但下一次你送出請求時會送到其他伺服器，回傳有效的資料。
 
 也就是說這樣的錯誤是隨機的，我們可以過幾秒再發送同樣的請求。後來主管教我用try else語法讓爬蟲偵測處理Error部分，待下次爬取時這個網址八成就會出現inspection結果了。

## 實驗發現

#### 1.URL檢查工具在Crawled與Indexed相關的狀態，改變速度跟不上實際上的索引結果

這次實驗中蒐集到的索引狀態變化如下表，照定義以為要出現**Indexed, not submitted in sitemap**或**Submitted and indexed**，才有被索引，但實際用site:的方式去找，在Discovered的時候，已可找到該網頁，從sitemap發布網址後到被google索引的時間約2分鐘左右。

不過要特別注意**URL is unknown to Google、Duplicate**, **submitted URL not selected as canonical**這兩種狀態，因為前者代表真的是沒被Google建索引
更多狀態說明可以參考search console的指引：[https://support.google.com/webmasters/answer/9012289?hl=zh-Hant](https://support.google.com/webmasters/answer/9012289?hl=zh-Hant)


| |狀態                                              |說明|
|-|--------------------------------------------------|---------|
|1|URL is unknown to Google                          |尚未被索引|
|2|Discovered - currently not indexed                |事實上已索引|
|3|Crawled - currently not indexed                   |事實上已索引，可查到上次檢索的時間|
|4|Indexed, not submitted in sitemap                 |已索引，有amp、richresult|
|4|Submitted and indexed                             |已索引，有amp、richresult|
| |Duplicate, submitted URL not selected as canonical|重複內容|
| |Alternate page with proper canonical tag          |重複內容，但已選了其他適合的作為索引結果|

#### 2.更新時間與爬蟲檢索頻率沒有絕對關係，且回報的檢索時間有問題

 文章更新次數可能會影響爬蟲檢索次數，但也有更新多次沒被多次檢索的。

 不過這次實驗沒有控制每筆資料查詢次數，且中間有修改過程式而暫停查詢、取消5分鐘休息時間等等，這些變因可能也影響資料完整。

 另外，發現google提供的最後爬取時間，有反覆出現的狀況，資料可信度待議。

 |爬蟲檢索次數|文章更新次數                                           |說明|
 |-|--------------------------------------------------|---------|
 |0|0|尚未被索引|有改網址，原網址無效／有次實驗的quota用完|
 |1|0-2||
 |2|0-3／6|有一次更新6次|
 |3|0-5／8|有一次更新8次|
 |4|0-5／8|有一次更新8次|
 |5|0-2||
 |6|4-6||

 ![時間反覆跳躍，頭痛](img\03_time.jpg "時間反覆跳躍，頭痛")
 
#### 3.重複內容無法完全用canonical來避免

 由於網站架構配置，有時會發布不同網址的重複內容，因應方式是改掉原網址的canonical換成新的網址，同時sitemap也會撤下原網址，放上新網址。

 但事實上google已收錄過原網址，在檢索的過程中會判斷新網址為重複內容（Duplicate, submitted URL not selected as canonical），此時的canonical尚未發生作用，最後可能是爬蟲爬了原網址，接收到canonical訊息後，也把新的網址給收進去了。

 但這讓我們反思，中間出現重複內容的狀態，是否會影響到網站評分？

 為什麼完全以canonical設定來避免呢？google的John在 #AskGoogleWebmasters 節目裡曾說過canonical選擇的標準包含：

 - Canonical宣告網址
 - 轉址網址
 - 內部連結
 - sitemap裡面的URL
 - 有Https的URL
 - 網址結構比較好的URL
 
 推斷canonical應是僅供參考，實際上google還是自己有判斷
 
 ?> 完整內容請看：[Canonical URLs: How Does Google Pick the One? #AskGoogleWebmasters](https://youtu.be/8j_hxBw5B4E)


抓下來的資料除了用spreadsheet整理，也有人開了data studio的模板來把數據視覺化，比較好看，但這個是以Screaming Frog輸出的資料來設計的，如果是直接用api抓可能需要調一下格式！

以上大概就是我們實（ㄨㄢˊ）驗（ㄕㄨㄚˇ）的過程與發現。當時碰上我們家PM要做某功能的SEO成效測試，覺得是個很不錯的嘗試機會，這大大增進我的寫Code技能，這套實驗也成功幫網站抓出一些陳年老Bug。

根據Inspection API提供的結果，大家可以更好地研擬適合自家網站的SEO優化策略，如果你使用完有什麼新發現，也歡迎留言和我們分享！

## 參考資源

- [inspection API twitter上的討論串](https://twitter.com/googlesearchc/status/1488097572459888640?s=20&t=GDR54FyGGq3E0jvhMzPaNg)

- [Google官方測試教學](https://developers.google.com/webmaster-tools/v1/urlInspection.index/inspect)

- [感謝澳洲SEO專家JC大大的完整教學](https://www.jcchouinard.com/google-url-inspection-api-with-python/)

- [Google Search Console Community的問題](https://support.google.com/webmasters/thread/150952847?hl=en)

### [demo]
from google.oauth2 import service_account
from google.oauth2.service_account import Credentials
from googleapiclient.discovery import build
### [demo]

### [abc]
creds = 'key.json' #此處填入你的json檔名
scopes = ['https://www.googleapis.com/auth/webmasters',
'https://www.googleapis.com/auth/webmasters.readonly']

credentials = service_account.Credentials.from_service_account_file(creds, scopes=scopes)
service = build('searchconsole','v1', credentials=credentials)
###[abc]


###[demo2]
#inspectionUrl = 你想檢索的網址；siteUrl = 主網站

request = {
  'inspectionUrl': 'https://www.cna.com.tw/news/afe/202203170150.aspx',
  'siteUrl': 'https://www.cna.com.tw/'
}

response = service.urlInspection().index().inspect(body=request).execute()
inspectionResult = response['inspectionResult']
print(inspectionResult)
###[demo2]